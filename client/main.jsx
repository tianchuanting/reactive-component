import { Meteor } from "meteor/meteor";
import React from "react";
import { render }  from "react-dom";

import { Todos } from "../collections/Todos";
import { App } from "../imports/ui/App.jsx";

Meteor.startup(() => {
  render(
      <App collections={ [ Todos ] } subscribeTo={ ["todos"] } />,
      document.getElementById("render-target")
  );
});
