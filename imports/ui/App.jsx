import { Meteor } from "meteor/meteor";
import React, { Component } from "react";
import ReactiveComponent from "meteor/louistian:reactive-component";


export class App extends ReactiveComponent {

  render () {
    return (
      <div>
        <h2>ToDo List</h2>
        <DisplayList todos={this.state.todos}/>
        <ListController />
      </div>
    )
  }

}

class DisplayList extends Component {
  render () {
    const self = this;
    return (
      <ul>
        {self.props.todos.map( x => {
          return <li key={x._id}>{x.text}</li>
        })}
      </ul>
    )
  }
}

class ListController extends Component {
  constructor () {
    super();
    this.state = {
      toDoText: ""
    }
  }

  syncChange (evt) {
    console.log(this)
    this.setState({toDoText: evt.target.value});
  }

  addToDo () {
    const self = this;
    console.log(this.state)
    Meteor.call("Todos.insert", {text: this.state.toDoText});
  }

  render () {
    return (
      <div>
        <input type="input" placeholder="new thing to do" value={this.state.toDoText} onChange={this.syncChange.bind(this)}/>
        {/* React autobind does not work with ES6 yet, need to bind this manually. */}
        <button onClick={this.addToDo.bind(this)}>+ TODO</button>
      </div>
    )
  }
}
