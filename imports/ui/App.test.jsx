import { Meteor } from "meteor/meteor";
import React from "react";
import { shallow } from "enzyme";
import { Todos } from "../../collections/Todos.jsx";
import { App } from "../../imports/ui/App.jsx";


describe("App", function () {

  afterEach( function () {
      Meteor.call("Todos.reset");
  });

  it('should have one entry in Todos', function () {
    expect(Todos.find({}).fetch().length).toEqual(0);
  });

  if (Meteor.isClient ) {
    it('should set component state using the collection', function () {
      const wrapper = shallow(
        <App collections={ [ Todos ] } subscribeTo={ ["todos"] }/>
      );

    Meteor.call(
        "Todos.insert",
        {text: "lorem Lpsum"}),
        (err, result) => {
            expect(err).toBeUndefined();
            expect(wrapper.state("todos")[0].text).toEqual("Lorem Lpsum");
        }
    });
  }

});
