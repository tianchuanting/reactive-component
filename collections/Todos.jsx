import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const Todos = new Mongo.Collection("todos");

Meteor.methods({
    "Todos.insert": doc => {
        Todos.insert(doc);
    },

    "Todos.reset": () => {
        Todos.remove({});
    }
})

if ( Meteor.isServer ) {
    Meteor.publish("todos", () => {
        return Todos.find({});
    })
}
