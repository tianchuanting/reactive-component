Package.describe({
  name: 'louistian:reactive-component',
  version: '1.0.0',
  // Brief, one-line summary of the package.
  summary: 'Simple Reactive Component',
  // URL to the Git repository containing the source code for this package.
  git: 'https://bitbucket.org/tianchuanting/reactive-component/overview',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.3.2.4');
  api.use('ecmascript');
  api.use('tmeasday:check-npm-versions@0.3.1');
  api.mainModule('reactive-component.js');
});
