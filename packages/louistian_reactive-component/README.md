# (React)ive-Component <small>Meteor reactivity augmented React component</small>#

Binding the client side Meteor Collection to React component's State for seamless reactivity UI updates

+ Simple, only 50 loc
+ Support subscription and auto stop subscription on component unmount

## API ##
``<ReactiveComponent collections={ [ MongoCollection, ... ] } subscribeTo={ [ "PublicationName", ... ] }``

*Note: pass the collection objects directly to the 'collection' attribute, and pass strings for subscribeTo.*

## Example

```jsx

=== Client & Server ==========================================================

import { Mongo } from "meteor/mongo";

export MyCollection = new Mongo.collection("myCollection");

if ( Meteor.isServer ) {
    Meteor.publish("publication", function () {
        return MyCollection.find({})
    });
}

=== Client ===================================================================
import React from "react";
import { Render } from "react-dom";
import ReactiveComponent from "louistian:reactive-component";
import { MyCollection } from "path/to/mycollection.js";

class MyComponent extends ReactiveComponent () {

    //the binded collections is accessible from this.props.collectionName

    render () {
        return {
            this.props.collectionName.map( ( doc ) => {
                return <div>doc.text</div>
            })    
        }
    }
}
Meteor.startup( () => {
    Render(<MyComponent collections= { [ MyCollection ] } subscribeTo={ ["publication"] }/>, )
})
```

See https://bitbucket.org/tianchuanting/reactive-component for a complete example
