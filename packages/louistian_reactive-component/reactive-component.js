import { Meteor } from "meteor/meteor";
import { Tracker } from "meteor/tracker";
import { Mongo } from "meteor/mongo";
import { checkNpmVersions } from "meteor/tmeasday:check-npm-versions";

checkNpmVersions({
  "react": ">=0.14.0"
}, "react")

let React = require("react");
const Component = React.Component;

export default class ReactiveComponent extends Component {

    constructor (...args) {
        super(...args);
        const self = this;

        // set initial state, can't use setState here because the component is not mounted yet.
        self.state = {};
        self.props.collections.forEach( collection => {
            self.state[collection._name] = [];
        });

        // subscribe to publication.
        self.subscribed = [];
        let subscribeTo = self.props.subscribeTo || []
        subscribeTo.forEach( subs => {
            //keep the handler around, so we can stop the subscription when unmounting the component
            self.subscribed.push(
                // subscribe to the publication
                Meteor.subscribe(subs)
            );
        })
    }

    componentDidMount () {
        const self = this;
        Tracker.autorun(()=>{
            self.props.collections.forEach( collection => {
                self.setState(
                    { [collection._name] : collection.find({}).fetch() }
                );
            });
        });
    }

    componentWillUnmount () {
        const self = this;
        self.subscribed.forEach( subs => {
            subs.stop();
        })
    }
}

ReactiveComponent.propTypes = {
    collections: React.PropTypes.arrayOf( React.PropTypes.instanceOf(Mongo.Collection) ),
    subscribeTo: React.PropTypes.arrayOf( React.PropTypes.string ) 
};
